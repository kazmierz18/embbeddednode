import cssnano from 'cssnano';
import cssnext from 'postcss-cssnext';
import nested from 'postcss-nested';
import simplevars from 'postcss-simple-vars';
import babel from 'rollup-plugin-babel';
import commonjs from 'rollup-plugin-commonjs';
import {eslint} from 'rollup-plugin-eslint';
import resolve from 'rollup-plugin-node-resolve';
import postcss from 'rollup-plugin-postcss';
import {uglify} from 'rollup-plugin-uglify';

export default {
  input: 'src/index.js',
  output: {
    file: 'build/index.min.js',
    format: 'iife',
    strict: false,
    sourceMap: false
  },
  external: [],
  plugins:
      [
        postcss({
          plugins: [
            simplevars(),
            nested(),
            cssnext({
              warnForDuplicates: false,
            }),
            cssnano(),
          ],
          extensions: ['.css']
        }),
        resolve({
          jsnext: true,
          module: true,
          next: true,
        }),
        commonjs(), eslint(), babel({
          exclude: 'node_modules/**',
        }),
        (process.env.NODE_ENV === 'production' && uglify())
      ]
};