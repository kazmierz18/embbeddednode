# Embedded NODE

This is my node package for developing simple js+css+html application for small embedded system

# Goals

1. Create workspace for easy to start development of web services
2. Minification and bundling for small memory targets, conserve space in FLASH and run time to send files over TCP/IP
3. Linters and compilation like checking for errors

All js source code is minified and bundled to one file to  decrease number of GET handlers in system
   
# Usage
1. Create your js css model in src
2. Run npm build
3. Upload to target
4. Debug
5. Run npm release
6. Upload to target